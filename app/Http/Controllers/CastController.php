<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function index()
    {
        $cast= DB::table('cast')->get();

        return view('tabel',['cast'=>$cast]);
    }

    public function create()
    {
        return view('create');
    }

    public function store(Request $ambil)
    {
        DB::table('cast')->insert([
            'nama'=>$ambil->nama,
            'umur'=>$ambil->umur,
            'bio'=>$ambil->bio
        ]);

        return redirect('/cast');
    }

    public function show($id)
    {
        $show= DB::table('cast')->where('id',$id)->get();

        return view('detail',['show'=>$show]);
    }

    public function edit($id)
    {
        $edit= DB::table('cast')->where('id',$id)->get();

        return view('edit',['edit'=>$edit]);
    }

    public function update(Request $ambil)
    {
        DB::table('cast')->where('id',$ambil->id)->update([
            'nama'=>$ambil->nama,
            'umur'=>$ambil->umur,
            'bio'=>$ambil->bio
        ]);

        return redirect('/cast');
    }

    public function destroy($id)
    {
        $query = DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast');
    }
}
