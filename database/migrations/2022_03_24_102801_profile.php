<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Profile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table){
            $table->bigIncrements('id');
            $table->string('nama', 45);
            $table->string('email', 45);
            $table->string('paswword', 45);
        });

        Schema::create('genre', function (Blueprint $table){
            $table->bigIncrements('id');
            $table->string('nama', 45);
        });

         Schema::create('cast', function (Blueprint $table){
            $table->bigIncrements('id');
            $table->string('nama', 45);
            $table->integer('umur');
            $table->text('bio');
        });

        Schema::create('profile', function (Blueprint $table){
            $table->bigIncrements('id');
            $table->integer('umur');
            $table->text('bio');
            $table->text('alamat');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('user');
        });

        Schema::create('film', function (Blueprint $table){
            $table->bigIncrements('id');
            $table->string('judul', 45);
            $table->text('ringkasan');
            $table->integer('tahun');
            $table->string('poster', 45);
            $table->unsignedBigInteger('genre_id');
            $table->foreign('genre_id')->references('id')->on('genre');
        });

        Schema::create('peran', function (Blueprint $table){
            $table->bigIncrements('id');
            $table->string('nama', 45);
            $table->unsignedBigInteger('film_id');
            $table->foreign('film_id')->references('id')->on('film');
            $table->unsignedBigInteger('cast_id');
            $table->foreign('cast_id')->references('id')->on('cast');
        });

        Schema::create('kritik', function (Blueprint $table){
            $table->bigIncrements('id');
            $table->text('content');
            $table->integer('point');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('user');
            $table->unsignedBigInteger('film_id');
            $table->foreign('film_id')->references('id')->on('film');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile');
    }
}
