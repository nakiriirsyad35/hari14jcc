@extends('master')

@section('judul','Edit Data')

@section('tabel')
	@foreach($edit as $id)
	<form method="POST" action="/cast/{id}">
		{{csrf_field()}}
        {{method_field('put')}} 
        <input type = "hidden" name = "id" value = "{{$id->id}}">
		<table>
			<tr>
				<td><label>Nama</label></td>
				<td><input type="text" name="nama" value="{{$id->nama}}"></td>
			</tr>
			<tr>
				<td><label>Umur</label></td>
				<td><input type="number" name="umur" value="{{$id->umur}}"><br></td>
			</tr>
			<tr>
				<td><label>Bio</label></td>
				<td><textarea name="bio">{{$id->bio}}</textarea><br></td>
			</tr>
			<br>
			<br>
			</table>
		<input type="submit" name="kirim" value="Perbarui Data">
	</form>
	@endforeach
@endsection