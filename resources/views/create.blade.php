@extends('master')

@section('judul','Tambah Cast')

@section('tabel')
	<form method="post" action="/cast">
		{{csrf_field()}}
		<input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>">
		<table>
			<tr>
				<td><label>Nama</label></td>
				<td><input type="text" name="nama"></td>
			</tr>
			<tr>
				<td><label>Umur</label></td>
				<td><input type="number" name="umur"><br></td>
			</tr>
			<tr>
				<td><label>Bio</label></td>
				<td><textarea name="bio"></textarea><br></td>
			</tr>
			<br>
			<br>
			</table>
		<input type="submit" name="kirim" value="Kirim">
	</form>
@endsection