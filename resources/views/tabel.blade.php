@extends('master')

@section('judul','Tabel Cast')

@section('tabel')
	<a href="/cast/create">Tambah</a>
	<table border="1">
		<tr>
			<th>Id</th>
			<th>Nama</th>
			<th>Umur</th>
			<th>Bio</th>
			<th colspan="3">Aksi</th>
		</tr>
		@foreach($cast as $id)
		<tr>
			<td>{{$id->id}}</td>
			<td>{{$id->nama}}</td>
			<td>{{$id->umur}}</td>
			<td>{{$id->bio}}</td>
			<td><a href="cast/show/{{ $id->id }}">Lihat Detail</a></td>
			<td><a href="cast/{{ $id->id }}/edit">Edit</a></td>
			<td><a href="cast/{{ $id->id }}/delete">Hapus</a></td>
		</tr>
		@endforeach
	</table>
@endsection

