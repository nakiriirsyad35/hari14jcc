@extends('master')

@section('judul','Detail Pemain')

@section('tabel')
	@foreach($show as $id)
	<a href="/cast">kembali</a>
	<br>
	<div class="card mb-3" style="max-width: 540px;">
	  <div class="row no-gutters">
	    <div class="col-md-4">
	      <img src="{{asset('img/help.png')}}" alt="..." style="width:150px; height: auto;">
	    </div>
	    <div class="col-md-8">
	      <div class="card-body">
	        <h5 class="card-title">{{$id->nama}}</h5>
	        <p class="card-text">{{$id->bio}}</p>
	        <p class="card-text"><small class="text-muted">Umur : {{$id->umur}}</small></p>
	      </div>
	    </div>
	  </div>
	</div>
	@endforeach
@endsection